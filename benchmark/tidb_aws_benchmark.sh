#!/bin/bash

outputfile=/home/ubuntu/git/ec-exercise-group3/benchmark/tidb_aws_eu_benchmark.txt

cd /home/ubuntu/git/ec-exercise-group3/ycsb-0.12.0/
echo "######################################################################" &>> $outputfile
echo "######################################################################" &>> $outputfile
echo "#######################  NEW BENCHMARK RUN  ##########################" &>> $outputfile
echo "######################################################################" &>> $outputfile
echo "######################################################################" &>> $outputfile

echo "Starting time of benchmark:" &>> $outputfile
date &>> $outputfile
echo " " &>> $outputfile

# Load workload command for tidb
#./bin/ycsb load jdbc -P jdbc-binding/conf/db.properties -P workloads/workloadb -cp jdbc-binding/lib/mysql-connector-java-5.1.18.jar -p recordcount=10000000 -p db.batchsize=1000 -threads 10 -s &>> $outputfile

# Execute benchmark command for tidb
./bin/ycsb run jdbc -P workloads/workloadb -P jdbc-binding/conf/db.properties -cp jdbc-binding/lib/mysql-connector-java-5.1.18.jar -p recordcount=10000000 -p operationcount=1000000 -threads 10 -s &>> $outputfile
