#!/bin/bash

outputfile=/home/ubuntu/git/ec-exercise-group3/benchmark/cloudspanner_us_benchmark.txt

cd /home/ubuntu/git/ec-exercise-group3/YCSB/

echo "######################################################################" &>> $outputfile
echo "######################################################################" &>> $outputfile
echo "#######################  NEW BENCHMARK RUN  ##########################" &>> $outputfile
echo "######################################################################" &>> $outputfile
echo "######################################################################" &>> $outputfile

echo "Starting time of benchmark:" &>> $outputfile
date &>> $outputfile
echo " " &>> $outputfile

# Load workload command for cloudspanner
#./bin/ycsb load cloudspanner -P cloudspanner/conf/cloudspanner.properties -P workloads/workloadb -p recordcount=10000000 -p cloudspanner.batchinserts=1000 -threads 10 -s &>> $outputfile

# Execute benchmark command for cloudspanner
./bin/ycsb run cloudspanner -P cloudspanner/conf/cloudspanner.properties -P workloads/workloadb -p recordcount=10000000 -p operationcount=1000000 -threads 10 -s &>> $outputfile
