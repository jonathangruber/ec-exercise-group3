var x = ["15.12.2017 17:58:51","15.12.2017 21:58:51","16.12.2017 01:58:51","16.12.2017 05:58:51","16.12.2017 09:58:51","16.12.2017 13:58:51","16.12.2017 17:58:51","16.12.2017 21:58:51","17.12.2017 01:58:51","17.12.2017 05:58:51","17.12.2017 09:58:51","17.12.2017 13:58:51"];
var cloudspanner = {
  "cseu": [313.51,520.60,461.90,481.18,314.77,452.70,445.59,455.27,462.36,315.52,316.70,316.90],
  "csus": [2627.75,2656.60,2759.76,2565.86,2880.39,2914.73,2791.04,254.86,2245.20,2824.37,2923.39,2503.39],
  "tidbgooeu": [446.44,417.51,428.81,456.86,438.87,422.11,448.95,454.40,437.93,443.14,466.08,452.29],
  "tidbgoous": [1281.73,1142.50,1346.83,1216.18,1386.29,1286.31,1334.00,1134.15,1251.65,1319.18,1325.30,1328.67],
  "tidbawseu": [2806.38,2856.58,2882.99,2878.89,2936.71,2924.85,2963.31,2942.56,2893.82,2903.31,2943.69,2934.21],
  "tidbawsus": [2807.93, 2825.24, 2817.16, 2797.32, 2752.76, 2772.21, 2765.63, 2765.74, 2753.47, 2787.61, 2773.02, 2781.29]
};

var trace1 = {
  y: cloudspanner.cseu,
  type: 'scatter',
  name: 'Cloudspanner EU'
};
var trace2 = {
  y: cloudspanner.csus,
  type: 'scatter',
  name: 'Cloudspanner US'
};
var trace3 = {
  y: cloudspanner.tidbgooeu,
  type: 'scatter',
  name: 'TiDB Google EU'
};
var trace4 = {
  y: cloudspanner.tidbgoous,
  type: 'scatter',
  name: 'TiDB Google US'
};
var trace5 = {
  y: cloudspanner.tidbawseu,
  type: 'scatter',
  name: 'TiDB AWS EU'
};
var trace6 = {
  y: cloudspanner.tidbawsus,
  type: 'scatter',
  name: 'TiDB AWS US'
};
var data = [trace1,trace2,trace3,trace4,trace5,trace6];
var layout = {
  title: 'CloudSpanner & TiDB Throughput Benchmark',
  xaxis: {
    title: 'Iterations'
  },
  yaxis: {
    title: 'Throughput (ops/sec)'
  }
};
Plotly.newPlot('cloudspanner', data, layout);