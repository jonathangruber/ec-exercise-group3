var x = ["15.12.2017 17:58:51","15.12.2017 21:58:51","16.12.2017 01:58:51","16.12.2017 05:58:51","16.12.2017 09:58:51","16.12.2017 13:58:51","16.12.2017 17:58:51","16.12.2017 21:58:51","17.12.2017 01:58:51","17.12.2017 05:58:51","17.12.2017 09:58:51","17.12.2017 13:58:51"];
var cloudspanner = {
  "eu": [313.51,520.60,461.90,481.18,314.77,452.70,445.59,455.27,462.36,315.52,316.70,316.90],
  "us": [2627.75,2656.60,2759.76,2565.86,2880.39,2914.73,2791.04,254.86,2245.20,2824.37,2923.39,2503.39]
};

var trace1 = {
  y: cloudspanner.eu,
  type: 'scatter',
  name: 'EU Region'
};
var trace2 = {
  y: cloudspanner.us,
  type: 'scatter',
  name: 'US Region'
};
var data = [trace1,trace2];
var layout = {
  title: 'CloudSpanner Benchmark',
  xaxis: {
    title: 'Iterations'
  },
  yaxis: {
    title: 'Throughput (ops/sec)'
  }
};
Plotly.newPlot('cloudspanner', data, layout);