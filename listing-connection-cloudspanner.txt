# perform this from root folder /YCSB

# connect to CloudSpanner
./bin/ycsb shell cloudspanner -P cloudspanner/conf/cloudspanner.properties

# load a workload (only do this once as it will upload 10GB of data into the db)
./bin/ycsb load cloudspanner -P cloudspanner/conf/cloudspanner.properties -P workloads/workloadb -p recordcount=10000000 -p cloudspanner.batchinserts=1000 -threads 10 -s

# perform a workload
./bin/ycsb run cloudspanner -P cloudspanner/conf/cloudspanner.properties -P workloads/workloadb -p recordcount=10000000 -p operationcount=1000000 -threads 10 -s