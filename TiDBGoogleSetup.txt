TiDB Setup

Used instances:

1x TiDB/PD node:

Node 1
IP Internal: 10.132.0.2
IP external: 35.205.196.207
CPU: 2 vCPUs
Memory: 16GB
Storage: 200GB Standard
Image: ubuntu 16.04 LTS
Zone: europe-west1-b
Cost: 0.129 $/hour

3x TiKV nodes:

Node 2
IP Internal: 10.132.0.3
IP external: 35.205.122.239
CPU: 2 vCPUs
Memory: 32GB
Storage: 200GB Standard
Image: ubuntu 16.04 LTS
Zone: europe-west2-b
Cost: 0.246 $/hour

Node 3
IP Internal: 10.132.0.4
IP external: 35.190.194.242
CPU: 2 vCPUs
Memory: 32GB
Storage: 200GB Standard
Image: ubuntu 16.04 LTS
Zone: europe-west3-b
Cost: 0.246 $/hour

Node 4
IP Internal: 10.132.0.5
IP external: 35.205.178.215
CPU: 2 vCPUs
Memory: 32GB
Storage: 200GB Standard
Image: ubuntu 16.04 LTS
Zone: europe-west4-b
Cost: 0.246 $/hour

Install TiDB on all 4 nodes:

# Download the package.
wget http://download.pingcap.org/tidb-latest-linux-amd64.tar.gz
wget http://download.pingcap.org/tidb-latest-linux-amd64.sha256

# Check the file integrity. If the result is OK, the file is correct.
sha256sum -c tidb-latest-linux-amd64.sha256

# Extract the package.
tar -xzf tidb-latest-linux-amd64.tar.gz
cd tidb-latest-linux-amd64

Start PD on Node 1
/home/chanchal1790/tidb-latest-linux-amd64/bin/pd-server --name=pd1 --data-dir=/home/chanchal1790/tidb-latest-linux-amd64/pd1 --client-urls="http://10.132.0.2:2379" --peer-urls="http://10.132.0.2:2380" --initial-cluster="pd1=http://10.132.0.2:2380" --log-file=/home/chanchal1790/tidb-latest-linux-amd64/pd.log

Start TiKV on Node 2
/home/chanchal1790/tidb-latest-linux-amd64/bin/tikv-server --pd="10.132.0.2:2379" --addr="10.132.0.3:20160" --data-dir=/home/chanchal1790/tidb-latest-linux-amd64/tikv1 --log-file=/home/chanchal1790/tidb-latest-linux-amd64/tikv.log

Start TiKV on Node 3
/home/chanchal1790/tidb-latest-linux-amd64/bin/tikv-server --pd="10.132.0.2:2379" --addr="10.132.0.4:20160" --data-dir=/home/chanchal1790/tidb-latest-linux-amd64/tikv2 --log-file=/home/chanchal1790/tidb-latest-linux-amd64/tikv.log

Start TiKV on Node 4
/home/chanchal1790/tidb-latest-linux-amd64/bin/tikv-server --pd="10.132.0.2:2379" --addr="10.132.0.5:20160" --data-dir=/home/chanchal1790/tidb-latest-linux-amd64/tikv3 --log-file=/home/chanchal1790/tidb-latest-linux-amd64/tikv.log

Start TiDB on Node 1
/home/chanchal1790/tidb-latest-linux-amd64/bin/tidb-server --store=/home/chanchal1790/tidb-latest-linux-amd64/tikv --path="10.132.0.2:2379" --log-file=/home/chanchal1790/tidb-latest-linux-amd64/tidb.log

Crontab for Node 1
@reboot /home/chanchal1790/tidb-latest-linux-amd64/bin/pd-server --name=pd1 --data-dir=/home/chanchal1790/tidb-latest-linux-amd64/pd1 --client-urls="http://10.132.0.2:2379" --peer-urls="http://10.132.0.2:2380" --initial-cluster="pd1=http://10.132.0.2:2380" --log-file=/home/chanchal1790/tidb-latest-linux-amd64/pd.log
@reboot sleep 90; /home/chanchal1790/tidb-latest-linux-amd64/bin/tidb-server --store=/home/chanchal1790/tidb-latest-linux-amd64/tikv --path="10.132.0.2:2379" --log-file=/home/chanchal1790/tidb-latest-linux-amd64/tidb.log

Crontab for Node 2
@reboot /home/chanchal1790/tidb-latest-linux-amd64/bin/tikv-server --pd="10.132.0.2:2379" --addr="10.132.0.3:20160" --data-dir=/home/chanchal1790/tidb-latest-linux-amd64/tikv1 --log-file=/home/chanchal1790/tidb-latest-linux-amd64/tikv.log

Crontab for Node 3
@reboot /home/chanchal1790/tidb-latest-linux-amd64/bin/tikv-server --pd="10.132.0.2:2379" --addr="10.132.0.4:20160" --data-dir=/home/chanchal1790/tidb-latest-linux-amd64/tikv2 --log-file=/home/chanchal1790/tidb-latest-linux-amd64/tikv.log

Crontab for Node 4
@reboot /home/chanchal1790/tidb-latest-linux-amd64/bin/tikv-server --pd="10.132.0.2:2379" --addr="10.132.0.5:20160" --data-dir=/home/chanchal1790/tidb-latest-linux-amd64/tikv3 --log-file=/home/chanchal1790/tidb-latest-linux-amd64/tikv.log
